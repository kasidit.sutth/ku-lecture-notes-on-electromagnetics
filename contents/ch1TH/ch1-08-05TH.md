
[$`\footnotesize \copyright 2020 \: \text {Denchai Worasawate}`$](https://www.researchgate.net/profile/Denchai-Worasawate)  


|[บทที่ 1](ch1TH.md)| [หน้าหลัก](../../README.md) | [Contents](../contentEN.md) | [สารบัญ](../contentTH.md) |
| ---------- | ---------- | ---------- | -------- |   

|[ก่อนหน้า](ch1-08-04TH.md)| [ต่อไป บทที่ 2](ch2TH.md) |
| ---------- | ---------- |    

### 1.8.5 [ตัวอย่างการแปลงเวกเตอร์ระหว่างพิกัดแบบทรงกระบอกกับพิกัดแบบทรงกลม](ch1-08-05TH.md)  

#### โจทย์  

#### วิธีทำ  


|[ก่อนหน้า](ch1-08-04TH.md)| [ต่อไป บทที่ 2](ch2TH.md) |
| ---------- | ---------- |    

[$`\footnotesize \copyright 2020 \: \text {Denchai Worasawate}`$](https://www.researchgate.net/profile/Denchai-Worasawate)  



